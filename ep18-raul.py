weight = int(input("Introduce the cargo's weight in tons: "))

if weight < 50:
    payment = weight * 1000
else:
    payment = weight * 1500

payment = payment + (payment * 0.13)

print("The total payment for " + str(weight) + " tons is equal to " + str(payment) + ".")

