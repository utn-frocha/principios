import math

intNumber = int(input("Introduce an int number: "))

while type(intNumber) != int:
    intNumber = int(input("Incorrect input, please introduce an int number: "))

if intNumber > 0:
    print("The square root of " + str(intNumber) + " is " + str(math.sqrt(intNumber)) + ".")
elif intNumber < 0:
    print("The square number of " + str(intNumber) + " is " + str(intNumber**2) + " and its cube is " + str(intNumber**3) + ".")
else:
    print("The number is 0 so I ain't calculating shit. :)")
