import sys

category = int(input("Introduce the employee's category. Type 1, 2 or 3: "))
currentSalary = int(input("Introduce the employee's current salary: "))

if category == 1:
    salaryIncrease = 0.15
elif category == 2:
    salaryIncrease = 0.2
elif category == 3:
    salaryIncrease = 0.25
else:
    sys.exit("Invalid input... Don't you know how to read?")

newSalary = currentSalary + (currentSalary * salaryIncrease)

print("The new salary is equal to " + str(newSalary))
