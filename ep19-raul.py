carPlate = int(input("Introduce the car's plate: "))
carYear = int(input("Introduce the car's model year: "))

if (carPlate >= 5000 and carPlate <= 8000) and (carYear == 1958 or carYear == 1959):
    print("This car might be interesting for the owner of the company.")
else:
    print("The company owner will not be interested in the car.")