anno = int(input("Ingrese un año para calcular si este es bisiesto o no: "))

if ((anno % 4) == 0) and ((anno % 100) != 0):
    print("El año " + str(anno) + " es bisiesto.")
else:
    print("El año " + str(anno) + " no es bisiesto.")
