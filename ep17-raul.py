landHeight = int(input("Introduce the land's height in meters: "))
landWidth = int(input("Introduce the land's width in meters: "))

print("Land's area is equal to " + str(landHeight * landWidth) + ".")

if landHeight * landWidth > 100:
    print("The land is appropriate for construction.")
else:
    print("The land is not appropriate for construction.")
