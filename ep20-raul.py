import sys

category = int(input("Introduce the hotel acommodation category. Type 1 for Luxury, 2 for Tourism and 3 for Business: "))
quantDays = int(input("Introduce the quantity of days to stay: "))

if category == 1:
    payment = quantDays * 200000
elif category == 2:
    payment = quantDays * 10000
elif category == 3:
    payment = quantDays * 75000
else:
    sys.exit("Invalid input... Don't you know how to read?")

payment = payment + (payment * 0.13)

print("The total payment for " + str(quantDays) + " days with " + str(category) + " category is equal to " + str(payment) + ".")
